<?php

/**
 * @file
 * Commerce Vauchar Admin Settings Form.
 */

/**
 * Function to get the settings for the Vauchar API.
 */
function commerce_vauchar_admin_settings() {
  $form = array();
  $vauchar_merchant_id = variable_get('commerce_vauchar_merchant_id');
  $form['commerce_vauchar_merchant_id'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Vauchar Merchant ID'),
    '#default_value' => $vauchar_merchant_id ? $vauchar_merchant_id : '',
  );

  $commerce_vauchar_api_key = variable_get('commerce_vauchar_api_key');
  $form['commerce_vauchar_api_key'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Vauchar API Key'),
    '#default_value' => $commerce_vauchar_api_key ? $commerce_vauchar_api_key : '',
  );

  return system_settings_form($form);
}
