<?php

/**
 * @file
 * Commerce Checkout pane implementations.
 */

/**
 * Checkout pane callback: commerce_vauchar checkout form.
 */
function commerce_vauchar_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Allow to replace pane content with ajax calls.
  $pane_form = array(
    '#prefix' => '<div id="commerce-vauchar-coupon-ajax-wrapper">',
    '#suffix' => '</div>',
  );

  $pane_form['vauchar_coupon_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Discount Code'),
    '#size' => 20,
    '#weight' => 1,
    '#description' => t('Enter your discount code here.'),
  );

  $pane_form['vauchar_coupon_add'] = array(
    '#type' => 'button',
    '#value' => t('Add discount'),
    '#name' => 'vauchar_coupon_add',
    '#validate' => array('commerce_vauchar_pane_checkout_form_coupon_validate'),
    '#limit_validation_errors' => array(),
    '#weight' => 3,
    '#ajax' => array(
      'callback' => 'commerce_vauchar_add_coupon_callback',
      'wrapper' => 'commerce-vauchar-coupon-ajax-wrapper',
    ),
  );

  if (isset($form_state['coupon'])) {
    $coupon = $form_state['coupon'];
    commerce_vauchar_apply_discount($order, $coupon);
    drupal_set_message(t('Discount code applied to cart'));
    $pane_form['vauchar_coupon_code']['#value'] = '';
    // Reload the order so it is not out of date.
    $order = commerce_order_load($order->order_id);
    // Recalculate discounts.
    commerce_cart_order_refresh($order);
  }

  $pane_form['redeemed_coupons'] = array(
    '#type'   => 'markup',
    '#markup' => commerce_vauchar_checkout_pane_view($order),
    '#weight' => 2,
  );

  $pane_form['vauchar_status_messages'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="commerce-vauchar-coupon-error-placeholder">',
    '#suffix' => '</div>',
    '#markup' => theme('status_messages'),
    '#weight' => -1,
  );

  return $pane_form;
}

/**
 * Ajax callback: coupon add button.
 */
function commerce_vauchar_add_coupon_callback($form, &$form_state) {
  // Re-render the cart summary.
  list($view_id, $display_id) = explode('|', variable_get('commerce_cart_contents_pane_view', 'commerce_cart_summary|default'));
  $cart_summary = commerce_embed_view($view_id, $display_id, array($form_state['order']->order_id));
  $commands[] = ajax_command_replace('.cart_contents .view', $cart_summary);
  // Re-render coupon pane.
  $coupon_pane = $form['commerce_vauchar'];
  $commands[] = ajax_command_remove('.messages');
  $commands[] = ajax_command_replace('#commerce-vauchar-coupon-ajax-wrapper', drupal_render($coupon_pane));
  if (isset($_SESSION['messages']['error'])) {
    $errorplaceholder = '.commerce-vauchar-coupon-error-placeholder';
    $commands[] = ajax_command_append($errorplaceholder, theme('status_messages'));
  }
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Function to get checkout pane view of applied discounts.
 */
function commerce_vauchar_checkout_pane_view($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $html = '';
  $container = '';
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    // If this line item is a vauchar type line item.
    if ($line_item_wrapper->getBundle() == 'commerce_vauchar') {
      $coupon_line_item = $line_item_wrapper->value();
      $coupon_code = check_plain($coupon_line_item->data['coupon_code']);
      $coupon_value = $coupon_line_item->data['coupon_value'];
      $remove_link = commerce_vauchar_discount_code_remove_link($coupon_code, $order);
      $html .= '<tr>';
      if ($coupon_line_item->data['coupon_unit'] == 'percentage') {
        $html .= "<b>$coupon_code</b> $coupon_value% off ($remove_link)";
      }
      else {
        $currency_code = $order_wrapper->commerce_order_total->currency_code->value();
        $html .= "<b>$coupon_code</b> $currency_code$coupon_value off ($remove_link)";
      }
      $html .= '</tr>';
    }
  }
  if ($html) {
    $container = '<table>';
    $container .= $html;
    $container .= '</table>';
  }
  return $container;
}
