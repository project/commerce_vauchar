DESCRIPTION
-----------

This module integrates commerce module with the Vauchar API
to give discounts to your customers using the coupons and vouchers
generated on Vauchar. For more details, visit http://vauchar.com


INSTALLATION
------------

1) Place this module directory in your "modules" folder (this will usually be
   "sites/all/modules/"). Don't install your module in Drupal core's "modules"
   folder, since that will cause problems and is bad practice in general. If
   "sites/all/modules" doesn't exist yet, just create it.

2) Enable the module.

3) Visit "admin/commerce/config/commerce_vauchar" to enter
   the merchant id and secret received from Vauchar.com
