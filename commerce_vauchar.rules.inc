<?php

/**
 * @file
 * Commerce Vauchar Rules Implementation.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_vauchar_rules_action_info() {
  $actions = array();
  $actions['commerce_vauchar_apply'] = array(
    'label' => t('Apply discount to an order'),
    'parameter' => array(
      'entity' => array(
        'label' => t('Entity'),
        'type' => 'entity',
        'wrapped' => TRUE,
      ),
      'coupon_code' => array(
        'type' => 'text',
        'label' => t('Coupon Code'),
        'description' => t('The code of the coupon which is applied to the order'),
      ),
      'coupon_value' => array(
        'type' => 'decimal',
        'label' => t('Coupon Value'),
        'description' => t('The value of the coupon'),
      ),
      'coupon_unit' => array(
        'type' => 'text',
        'label' => t('Coupon Unit'),
        'description' => t('The unit of the coupon which is applied to the order'),
      ),
      'coupon_id' => array(
        'type' => 'text',
        'label' => t('Coupon Id'),
        'description' => t('The id of the coupon which is applied to the order'),
      ),
      'coupon_type' => array(
        'type' => 'text',
        'label' => t('Coupon Type'),
        'description' => t('The type of the coupon which is applied to the order'),
      ),
    ),
    'group' => t('Commerce Vauchar'),
  );
  return $actions;
}

/**
 * Action: Apply the discount to an order.
 */
function commerce_vauchar_apply(EntityDrupalWrapper $order_wrapper, $coupon_code, $coupon_value, $coupon_unit, $coupon_id, $coupon_type) {
  $order = $order_wrapper->value();
  $currency_code = $order_wrapper->commerce_order_total->currency_code->value();
  commerce_vauchar_delete_discount_line_item($order, TRUE);
  $amount = -commerce_vauchar_calculate_order_discount($order_wrapper, $coupon_unit, $coupon_value);
  // Built the unit_price.
  $base_rate = array(
    'amount' => $amount,
    'currency_code' => $currency_code,
    'data' => array(),
  );
  $base_rate['data'] = commerce_price_component_add($base_rate, 'commerce_vauchar', $base_rate, TRUE, FALSE);
  // Delete any existing commerce_vauchar line items with the same name from
  // the order.
  $data = array(
    'coupon_code' => $coupon_code,
    'coupon_value' => $coupon_value,
    'coupon_unit' => $coupon_unit,
    'coupon_id' => $coupon_id,
    'coupon_type' => $coupon_type,
  );
  // Create a new commerce_vauchar line item with the amount from the form.
  $line_item = commerce_vauchar_line_item_new($base_rate, $order->order_id, $data);
  // Save and add the line item to the order.
  commerce_vauchar_add_discount_line_item($line_item, $order, TRUE);

  commerce_order_save($order);
}
