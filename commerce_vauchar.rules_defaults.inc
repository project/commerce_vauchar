<?php

/**
 * @file
 * Commerce Vauchar Rules Implementation.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_vauchar_default_rules_configuration() {
  $config = array();
  $rule = '{ "rules_commerce_vauchar_apply_discount" : {
       "LABEL" : "Commerce Vauchar Apply Discounts",
       "PLUGIN" : "action set",
       "OWNER" : "rules",
       "REQUIRES" : [ "commerce_vauchar" ],
       "USES VARIABLES" : {
         "order" : { "label" : "order", "type" : "commerce_order" },
         "coupon_code" : { "label" : "Coupon Code", "type" : "text" },
         "coupon_value" : { "label" : "Coupon Value", "type" : "decimal" },
         "coupon_unit" : { "label" : "Coupon Unit", "type" : "text" },
         "coupon_id" : { "label" : "Coupon ID", "type" : "text" },
         "coupon_type" : { "label" : "Coupon Type", "type" : "text" }
       },
       "ACTION SET" : [
         { "commerce_vauchar_apply" : {
             "entity" : [ "order" ],
             "coupon_code" : [ "coupon_code" ],
             "coupon_value" : [ "coupon_value" ],
             "coupon_unit" : [ "coupon_unit" ],
             "coupon_id" : [ "coupon_id" ],
             "coupon_type" : [ "coupon_type" ]
           }
         }
       ]
     }
   }';
  $config['commerce_vauchar_apply_discount'] = rules_import($rule);
  return $config;
}
